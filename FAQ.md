# Frequently Asked Questions

## Notation

### Q1: What does notation such as the following mean?

$`f(x_j) = P(X = x_j)`$ for $`j = 1,\ldots,k`$

A: This is shorthand notation that is equivalent to the following:

$`f(x_1) = P(X = x_1)`$

$`f(x_2) = P(X = x_2)`$

$`f(x_3) = P(X = x_3)`$

$`\vdots`$

$`f(x_k) = P(X = x_k)`$

Note that $`j`$ can be a different letter, as long as it is replaced
in all places. For example, the following is equivalent to the above:

$`f(x_b) = P(X = x_b)`$ for $`b = 1,\ldots,k`$

However, $`k`$ cannot be replaced. It has a certain "external" meaning.
In this case, if we are talking about the probability mass function, $`k`$
is the number of distinct values that the r.v. $`X`$ can take. Thus, you cannot
just replace $`k`$ with a different letter as we replaced $`j`$ with $`b`$
above.


### Q2: Why are you using $`g`$ now if in the definition you used $`f`$?

$`g`$ and $`f`$ are just letters. They can mean one thing on one side of the
board, and something different on the other. For example, I might write the
following on the board:

>  Let $`g`$ be a function. Define $`g`$ to be a *_positive function_* if
>  $`g(x) > 0`$ for all $`x`$.

Then, suppose that I write the following:

>  Define the function $`f`$ such that $`f(x) = 3*x^2 + 3`$

Is $`f`$ a "positive function", even though the letter $`f`$ is not the same as the letter $`g`$ ? Yes, it is.
