# Dots and Hats

This repository contains additional materials on probability, statistics,
and R for economists at the University of Florida.

# Properties of the Summation Operator

https://en.wikibooks.org/wiki/Econometric_Theory/Summation_and_Product_Operators

http://www.statpower.net/Content/310/Summation%20Algebra.pdf

https://tutorial.math.lamar.edu/classes/calci/summationnotation.aspx

https://www.youtube.com/watch?v=XJkIaw2e1Pw

Alternative proof that the sum of deviations from the mean is zero: https://www.youtube.com/watch?v=CNHOArE2YRw

# Resources for Experiments, Sample Spaces, and Events

Here are two videos that seem helpful:

Part 1: https://www.youtube.com/watch?v=TBgCZjJ1Soc
Part 2: https://www.youtube.com/watch?v=KxdUErrQ764


Here are some other videos in case you would like different introductions:

https://www.youtube.com/watch?v=W-tWFDkHYXk
https://www.youtube.com/watch?v=SkidyDQuupA
https://www.youtube.com/watch?v=M5_oEkk4X_g

The following is an advanced (for an undergraduate level) book, but I link to it here since it is open access (i.e., free to download) and those of you who may want to study probability or economics in graduate school may be interested:

https://autotomic.wixsite.com/markhubernews/probability-textbook

# Installing R and RStudio

R and "RStudio Desktop" are both free and open source. You must first install R
and then RStudio.

## Installing R

If you use Windows, click on "Download R \<version\> for Windows"
[here](https://mirrors.nics.utk.edu/cran/bin/windows/base/).

If you use macOS, download the appropriate .pkg file for your macOS version
[here](https://mirrors.nics.utk.edu/cran/bin/macosx/).

If you use Ubuntu, you can run the following:

```bash
sudo apt-get install r-base
```

If you use a different distribution of Linux, use the package manager
accordingly.

## Installing RStudio

After installing R above, install the free version of RStudio Desktop
[here](https://www.rstudio.com/products/rstudio/download/#download). Choose the
download that corresponds to your operating system.

# Resources for Learning R

## The Art of R Programming
https://nostarch.com/artofr.htm

You can access the book here:
http://diytranscriptomics.com/Reading/

## R for Data Science
https://www.amazon.com/Data-Science-Transform-Visualize-Model/dp/1491910399/

Available for free here: 
http://r4ds.had.co.nz/index.html

## Data Camp

The entire "Introduction to R" is free:

https://www.datacamp.com/courses/free-introduction-to-r

The first chapter of many of the other courses is also free. For example,
the first chapter of the intermediate R course is useful:

https://www.datacamp.com/courses/intermediate-r

For $25/month, you can get access to all courses. For more information, see [pricing](https://www.datacamp.com/pricing).

## Specific to Economics

[Introduction to R for Economists (videos)](https://www.youtube.com/playlist?list=PLcTBLulJV_AIuXCxr__V8XAzWZosMQIfW)
